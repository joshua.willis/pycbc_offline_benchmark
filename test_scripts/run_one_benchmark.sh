#!/bin/bash
#
# Copyright 2022 California Institute of Technology
#
# You should have received a copy of the licensing terms for this
# software included in the file “LICENSE” located in the top-level
# directory of this package. If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE.txt

scheme=$1
outfile=$2
wisfile=$3
inspiral_exec="fake_inspiral"

mypy=`which python`
date
echo "My python is ${mypy}"
echo "Checking CPU affinity"
taskset -p $BASHPID
$inspiral_exec $scheme $outfile $wisfile
echo "Driver process exiting"
exit 0
