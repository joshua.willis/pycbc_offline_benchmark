#!/bin/bash
#
# Copyright 2022 California Institute of Technology
#
# You should have received a copy of the licensing terms for this
# software included in the file “LICENSE” located in the top-level
# directory of this package. If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE.txt


mypy=`which python`
date
echo "My python is ${mypy}"
echo "Checking CPU affinity"
taskset -p $BASHPID
while true
do
    sleep 10
done
echo "Process exiting"
exit 0
