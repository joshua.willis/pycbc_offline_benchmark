#!/bin/bash
#
# Copyright 2022 California Institute of Technology
#
# You should have received a copy of the licensing terms for this
# software included in the file “LICENSE” located in the top-level
# directory of this package. If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE.txt

wisfile=$1

mypy=`which python`
date
echo "My python is ${mypy}"
echo "Checking CPU affinity"
taskset -p $BASHPID
if [[ "x${wisfile}" == "x" ]] ; then
    echo "Error: cannot provide empty wisdom file!"
    exit 1
else
    echo "Selected wisdom file is ${wisfile}"
    echo "42" > ${wisfile}
fi
sleep 20
echo "Process exiting"
exit 0
