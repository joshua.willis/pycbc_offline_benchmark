#!/usr/bin/env python3
#
# Copyright 2022 California Institute of Technology
#
# You should have received a copy of the licensing terms for this
# software included in the file “LICENSE” located in the top-level
# directory of this package. If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE.txt

import pycbc
from pycbc import fft
from pycbc.types import zeros, Array, complex64

# This program is designed to run without stopping, intended to be run
# on all other cores when wisdom is being created on one, so that such
# wisdom is created on a fully saturated machine.

fft.backend_support.set_backend(["fftw"])
fft.fftw.set_measure_level(0)

print("FFT backend = {0}".format(fft.backend_support.get_backend()))
print("fft.fftw.float_lib = {0}".format(fft.fftw.float_lib))

N = 1024*1024

a = zeros(N, complex64)
b = zeros(N, complex64)
while True:
    fft.ifft(a,b)
