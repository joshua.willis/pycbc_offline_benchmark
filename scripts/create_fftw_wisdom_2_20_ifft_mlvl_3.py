#!/usr/bin/env python3
#
# Copyright 2022 California Institute of Technology
#
# You should have received a copy of the licensing terms for this
# software included in the file “LICENSE” located in the top-level
# directory of this package. If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE.txt

import pycbc
from pycbc import fft
from pycbc.types import zeros, Array, complex64
import argparse

parser = argparse.ArgumentParser(usage='',
                   description="Create FFTW single precision wisdom at" 
                               " measure level 3, IFFT, 2^20 length C2C")

fft.insert_fft_option_group(parser)
opt = parser.parse_args()
fft.verify_fft_options(opt, parser)
fft.from_cli(opt)

# The whole point of this program is to export wisdom, so fail if no
# output file given

if opt.fftw_output_float_wisdom_file is None:
    raise RuntimeError("You must specify a file in which to save single"
                       " precision wisdom via "
                       "--fftw-output-float-wisdom-file <filename>")

# No matter what we've been told, we use the FFTW backend
# and the highest measure level

fft.backend_support.set_backend(["fftw"])
fft.fftw.set_measure_level(3)

print("FFT backend = {0}".format(fft.backend_support.get_backend()))
print("fft.fftw.float_lib = {0}".format(fft.fftw.float_lib))

N = 1024*1024

a = zeros(N, complex64)
b = zeros(N, complex64)
fft.ifft(a,b)
fft.fftw.export_single_wisdom_to_filename(opt.fftw_output_float_wisdom_file)
