#!/usr/bin/env python3
#
# Copyright 2022 California Institute of Technology
#
# You should have received a copy of the licensing terms for this
# software included in the file “LICENSE” located in the top-level
# directory of this package. If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE.txt

import argparse
import h5py
import json
from glob import glob
import re
from os import path, getcwd
from numa_benchmarks.utils import subdir_prefix

result_name="pycbc_offline_benchmark_results.json"

parser = argparse.ArgumentParser(
           description="Extract benchmarking data from each result file"
           " in  the subdirectories of a benchmarking run, and collate"
           " the results into the output json file",
           formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument("--subdir-prefix", type=str, default=subdir_prefix,
                    help="Prefix in which to search for directories of"
                    " the form <subdir_prefix>_job<n> for integers n."
                    " Each such directory will be searched for a"
                    " benchmarking output file, with an error if such"
                    " a file is not found. The number after 'job' at"
                    " the end will be used as the job number. All such"
                    " directories in the current working directory will"
                    " be searched.")
parser.add_argument("--outfile-basename", type=str, default=None,
                    help="The name of the file that must be present in"
                    " each working subdirectory, and which will be"
                    " opened for reading to extract the templates per"
                    " core.")
parser.add_argument("--result-file", type=str, default=result_name,
                    help="The output JSON file into which to write"
                    " the collated results from each benchmarking"
                    " run. Will be placed in the current directory.")

opts = parser.parse_args()

if opts.outfile_basename is None:
    parser.error("You must specify the output file basename")

basedir = getcwd()
benchmark_dirs = glob(path.join(basedir, opts.subdir_prefix + '_job*'))

if len(benchmark_dirs) == 0:
    raise RuntimeError("No subdirectories of form '{0}_job<n>'"
                       " in the current working directory".format(opts.subdir_prefix))

i = 0
jdict = { "job_number": {}, "templates_per_core": {} }
mpattern = '^.+_job(\d+)$'

for wdir in benchmark_dirs:
    m = re.search(mpattern, path.basename(wdir))
    if m is not None:
        jnum = int(m.group(1))
    else:
        raise RuntimeError("Benchmark directory {0} does not end with"
                           " '_job<n>' for some number <n>".format(wdir))
    f = h5py.File(path.join(wdir, opts.outfile_basename), "r")
    ifo = list(f.keys())[0]
    tpc = f[ifo]['search']['templates_per_core'][:][0]
    si = str(i)
    jdict["job_number"].update({si: jnum})
    jdict["templates_per_core"].update({si:tpc})
    f.close()
    i += 1

with open(path.join(basedir, opts.result_file), "w") as OutputFile:
    json.dump(jdict, OutputFile)

