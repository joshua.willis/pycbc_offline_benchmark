#!/bin/bash
#
# Copyright 2022 California Institute of Technology
#
# You should have received a copy of the licensing terms for this
# software included in the file “LICENSE” located in the top-level
# directory of this package. If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE.txt

# Exit on error:
set -e

CMDLINE=`getopt -o htans --long help,test,allow-underutilize,non-local-mem,split-nodes -n 'pycbc_benchmark_orchestrator' -- "$@"`

if [ $? != 0 ] ; then echo "Exiting" >&2 ; exit 1 ; fi

eval set -- "$CMDLINE"

testmode=0
allow_underuse=0
non_local=0
split_nodes=0

while true; do
    case "$1" in
	-t | --test ) testmode=1; shift ;; 
	-a | --allow-underutilize ) allow_underuse=1 ; shift ;;
	-n | --non-local-mem ) non_local=1 ; shift ;;
	-s | --split-nodes ) split_nodes=1 ; shift ;;
        -h | --help )
	    echo "Run the appropriate pycbc benchmarks on the current"
	    echo "machine, as selected by the  'BMK_PLATFORM' environment"
	    echo "variable.  As necessary, this will create FFTW wisdom"
	    echo "for the complex-to-complex 2^20 inverse FFT that the"
	    echo "offline configuration of 'pycbc_inspiral' uses, and"
	    echo "then run a sample inspiral job, one for each CPU of"
	    echo "the machine, and finally extract the templates-per-core"
	    echo "figure-of-merit from each such run."
	    echo
	    echo "Since it will not always be clear which FFT library"
	    echo "will be optimal for particular hardware, this benchmark"
	    echo "attempts to try everything that could be reasonable"
	    echo "for the specified platform. For the AMD platform, that"
	    echo "is two versions of the FFTW library. The 'fftw-igwn-opt'"
	    echo "library is the current FFTW compiled with maximal"
	    echo "vectorization options for the planner to consider; more"
	    echo "than what is included in the standard conda build of"
	    echo "FFTW. The 'amd-fast-plan' library is the AMD"
	    echo "customized version of FFTW, configured with the fast"
	    echo "planning option.  For intel, the two choices are again"
	    echo "the 'fftw-igwn-opt' benchmark, and the 'mkl' benchmark."
	    echo "The latter uses MKL for its FFT library, and will not"
	    echo "need a wisdom planning stage."
	    echo
	    echo "The orchestrator first creates and changes into a"
	    echo "directory 'pycbc_offline_benchmark_<date>_<time>."
	    echo "Within this, it creates a platform directory and"
	    echo "enters that, and then finally it will create two"
	    echo "directories for whichever two FFT libraries are to be"
	    echo "used. Each such directory will have a subdirectory"
	    echo "'wisdom', if it is an FFTW compilation, and all"
	    echo "library subdirectories will have a subdirectory"
	    echo "'benchmark', under which each benchmark job will write"
	    echo "its results. It is in the benchmark directory that the"
	    echo "overall results will be stored."
	    echo
	    echo "Usage: pycbc_benchmark_orchestrator.sh <options>"
	    echo
	    echo "Optional arguments:"
	    echo "  -t | --test                Enable test mode, which uses stand-in"
	    echo "                             executables that do no real work."
            echo "  -a | --allow-underutilize  Allow some cpus to be unassigned to"
	    echo "                             any jobs."
	    echo "  -n | --non-local-mem       Allow jobs bound by their cpu to one"
	    echo "                             NUMA memory node to allocate memory"
	    echo "                             from any NUMA node."
	    echo "  -s | --split-nodes         Allow multi-threaded jobs to be"
	    echo "                             assigned to cpus from more than one"
	    echo "                             NUMA node."
	    echo "  -h | --help         Print this help message and exit"
	    exit 0 ;;
	--) shift ; break ;;
        * ) echo "Error: unrecognized argument" ; exit 1 ;;
    esac
done

platform="${BMK_PLATFORM}"

startdir="${PWD}"
basedir="${PWD}/pycbc_offline_benchmark_$(date +%Y%m%d_%H_%M_%S)/${platform}"
echo "$(date +%c): Creating top-level directory ${basedir}"
mkdir -p $basedir; cd $basedir

source /bmk/pycbc_offline/bin/frame_defs.sh

if [[ ! -r "${FRAME1}" ]] ; then
    echo "$(date +%c): Error: ${FRAME1} does not exist or does not have read permission"
    echo "$(date +%c): Did you bind the correct host location into /cvmfs in the container?"
    exit 1
fi

if [[ ! -r "${FRAME2}" ]] ; then
    echo "$(date +%c): Error: ${FRAME2} does not exist or does not have read permission"
    echo "$(date +%c): Did you bind the correct host location into /cvmfs in the container?"
    exit 1
fi


if [[ "${platform}" == "amd" ]] ; then
    fftlibs=("fftw_igwn_opt" "fftw_amd_fast_plan")
else
    fftlibs=("fftw_igwn_opt" "fft_mkl")
fi

wisdomlibs=("fftw_igwn_opt" "fftw_amd_fast_plan")
declare -A SCHEMES=(['fftw_igwn_opt']='cpu' \
    ['fftw_amd_fast_plan']='cpu' \
    ['fft_mkl']='mkl')

source /opt/conda/etc/profile.d/conda.sh
if [[ $testmode -ne 0 ]] ; then
    echo "$(date +%c): Running in test mode"
    export PATH="/bmk/pycbc_offline/bin/test:${PATH}"
    outfile_name="sample_output.hdf"
    verbose="--verbose"
else
    outfile_name="pycbc_offline_bmk_output.hdf"
fi

if [[ $allow_underuse -eq 0 ]] ; then
    underutilize="--forbid-underutilize"
fi

if [[ $non_local -eq 0 ]] ; then
    localmem="--only-local-mem"
fi

if [[ $split_nodes -ne 0 ]] ; then
    splitnodes="--allow-split-nodes"
fi

benchmark_driver=`which run_one_benchmark.sh`
wisdom_creator=`which wisdom_driver.sh`
busywork_wisdom=`which endless_fftw_2_20_ifft.py`
results_parser=`which parser_driver.sh`

for flib in ${fftlibs[@]};
do
    echo "$(date +%c): Activating conda environment ${flib}"
    conda activate "${flib}"
    echo "$(date +%c): Creating subdirectory for FFT library ${flib}"
    mkdir $flib; pushd $flib > /dev/null
    if [[ " ${wisdomlibs[*]} " =~ " ${flib} " ]] ; then
	mkdir "wisdom"; pushd "wisdom" > /dev/null
	wisfile="${PWD}/fftw_c2c_ifft_2_20.wis"
	echo "$(date +%c): Generating wisdom file ${wisfile}; this may take some time!"
	"${CONDA_PREFIX}/bin/single_busywork" --executable ${wisdom_creator} \
	    --exec-args ${wisfile} --busy-executable ${busywork_wisdom} \
	    ${splitnodes} ${underutilize} ${localmem} ${verbose} --manager-exec "numactl"
	popd > /dev/null
    fi
    echo "$(date +%c): Running primary benchmark"
    mkdir "benchmark"; pushd "benchmark" >/dev/null
    "${CONDA_PREFIX}/bin/drive_benchmark" --executable ${benchmark_driver} \
	--exec-args ${SCHEMES[${flib}]} ${outfile_name} ${wisfile} \
	${splitnodes} ${underutilize} ${localmem} ${verbose} --manager-exec "numactl"
    echo "$(date +%c): Parsing results of primary benchmark"
    ${results_parser} ${outfile_name}
    popd >/dev/null # From 'benchmark'
    popd >/dev/null # From ${flib} subdir
    echo "$(date +%c): Deactivating conda environment ${flib}"
    conda deactivate
done

exit 0

