#!/bin/bash
#
# Copyright 2022 California Institute of Technology
#
# You should have received a copy of the licensing terms for this
# software included in the file “LICENSE” located in the top-level
# directory of this package. If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE.txt

scheme=$1
outfile=$2
if [[ $scheme == "cpu" ]] ; then
    fft_wisdom_file="--fftw-input-float-wisdom-file $3"
fi

echo "Checking CPU affinity"
taskset -p $BASHPID

FRAME1="/cvmfs/gwosc.osgstorage.org/gwdata/O3a/strain.16k/frame.v1/H1/1241513984/H-H1_GWOSC_O3a_16KHZ_R1-1242443776-4096.gwf"
FRAME2="/cvmfs/gwosc.osgstorage.org/gwdata/O3a/strain.16k/frame.v1/H1/1241513984/H-H1_GWOSC_O3a_16KHZ_R1-1242439680-4096.gwf"
GATING_FILE="/bmk/pycbc_offline/data/files/H1-O3_GATES_1238166018-31197600.txt"
BANK_FILE="/bmk/pycbc_offline/data/files/subbank_O3_H1L1_OPT_FLOW_HYBRID_BANK_compressed_2048hz_512s_1em6tol_flat_unity_psd.hdf"
source /bmk/pycbc_offline/bin/frame_defs.sh

pycbc_inspiral --pad-data 8 --strain-high-pass 15 --sample-rate 2048 --segment-length 512 --segment-start-pad 144 --segment-end-pad 16 --allow-zero-padding  --taper-data 1 --psd-estimation median --psd-segment-length 16 --psd-segment-stride 8 --psd-inverse-length 16 --psd-num-segments 63 --psdvar-segment 8 --psdvar-short-segment 0.25 --psdvar-long-segment 512 --psdvar-psd-duration 8 --psdvar-psd-stride 4 --psdvar-low-freq 20 --psdvar-high-freq 480 --autogating-threshold 50 --autogating-cluster 0.1 --autogating-width 0.25 --autogating-taper 0.25 --autogating-pad 16 --autogating-max-iterations 5 --use-compressed-waveforms  --waveform-decompression-method 'inline_linear' --enable-bank-start-frequency  --low-frequency-cutoff 20 --approximant 'SPAtmplt:mtotal<4' 'SEOBNRv4_ROM:else' --order -1 --snr-threshold 4.0 --keep-loudest-interval 1.072 --keep-loudest-num 100 --keep-loudest-stat newsnr_sgveto --cluster-window 1 --cluster-function symmetric --chisq-snr-threshold 5.25 --chisq-bins "0.72*get_freq('fSEOBNRv4Peak',params.mass1,params.mass2,params.spin1z,params.spin2z)**0.7" --newsnr-threshold 4.0 --sgchisq-snr-threshold 6.0 --sgchisq-locations "mtotal>30:20-15,20-30,20-45,20-60,20-75,20-90,20-105,20-120" --finalize-events-template-rate 500 --processing-scheme ${scheme} ${fft_wisdom_file} --channel-name H1:GWOSC-16KHZ_R1_STRAIN --gps-start-time 1242440327 --gps-end-time 1242444007 --trig-start-time 1242440477 --trig-end-time 1242443911 --output ${outfile} --bank-file ${BANK_FILE} --frame-files ${FRAME1} ${FRAME2} --gating-file ${GATING_FILE}
