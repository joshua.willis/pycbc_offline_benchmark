FROM igwn/base:el8

COPY conda_spec_files/conda.repo /etc/yum.repos.d/conda.repo
COPY docker/etc/cvmfs/default.local /etc/cvmfs/default.local
COPY docker/etc/cvmfs/60-osg.conf /etc/cvmfs/60-osg.conf
COPY docker/etc/cvmfs/config-osg.opensciencegrid.org.conf /etc/cvmfs/config-osg.opensciencegrid.org.conf

RUN rpm --import https://repo.anaconda.com/pkgs/misc/gpgkeys/anaconda.asc
RUN dnf -y install https://ecsft.cern.ch/dist/cvmfs/cvmfs-release/cvmfs-release-latest.noarch.rpm && \
    dnf -y install cvmfs cvmfs-config-default && dnf clean all					  
RUN dnf -y install conda which numactl && dnf clean all

COPY conda_spec_files/pycbc_fftw_amd_fast_plan /etc/conda/pycbc_fftw_amd_fast_plan
COPY conda_spec_files/pycbc_fftw_igwn_opt /etc/conda/pycbc_fftw_igwn_opt
COPY conda_spec_files/pycbc_mkl /etc/conda/pycbc_mkl
COPY scripts/* /bmk/pycbc_offline/bin/
COPY test_scripts/* /bmk/pycbc_offline/bin/test/
COPY docker/etc/cvmfs/frame_defs.sh /bmk/pycbc_offline/bin

RUN mkdir -p /bmk/pycbc_offline/data/files/test
RUN curl -SL "https://ldas-jobs.ligo.caltech.edu/~joshua.willis/benchmarking_support/pycbc/offline/O3test/sample_output.hdf" --output /bmk/pycbc_offline/data/files/test/sample_output.hdf
RUN curl -SL "https://ldas-jobs.ligo.caltech.edu/~joshua.willis/benchmarking_support/pycbc/offline/O3test/H1-O3_GATES_1238166018-31197600.txt" --output /bmk/pycbc_offline/data/files/H1-O3_GATES_1238166018-31197600.txt
RUN curl -SL "https://ldas-jobs.ligo.caltech.edu/~joshua.willis/benchmarking_support/pycbc/offline/O3test/subbank_O3_H1L1_OPT_FLOW_HYBRID_BANK_compressed_2048hz_512s_1em6tol_flat_unity_psd.hdf" --output /bmk/pycbc_offline/data/files/subbank_O3_H1L1_OPT_FLOW_HYBRID_BANK_compressed_2048hz_512s_1em6tol_flat_unity_psd.hdf

RUN mkdir -p /cvmfs/gwosc.osgstorage.org && \
    echo "gwosc.osgstorage.org /cvmfs/gwosc.osgstorage.org cvmfs ro,noauto 0 0" >> /etc/fstab

ENV PATH /bmk/pycbc_offline/bin:$PATH


