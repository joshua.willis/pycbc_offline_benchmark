
### Overview

This repository provides a benchmarking suite for PyCBC in its "offline"
mode, as it is usually run for the high-latency search. It uses a recent
(at the time of this writing, version 2.0.2) PyCBC to analyze the time 
around GW190521 in the H1 detector, using GWOSC frames.

The benchmark relies on the [numa_benchmarks](https://git.ligo.org/joshua.willis/numa_benchmarks)
Python package to manage its processes. This package queries the machine
on which it runs to determine how many cpus are present (note that
`lscpu` uses the term 'cpu' for what many might term a 'core'; `numa_benchmarks`
tries to follow the `lscpu` terminology since that is what it uses
internally to query the machine) and on what NUMA nodes they are layed out.
This particular benchmark is single-threaded, so it will start one copy
of the benchmark for each cpu, and will set the CPU affinity of each
process to its assigned cpu.

Additionally, it will also force each
process to only allocate memory from the NUMA node on which it runs;
if any process requires more memory than available it will fail. This
can be disabled by passing the `-n` option at the end of the appropriate
`docker run` line below. This is probably not what you want: if a
process cannot allocate memory from its local node, then since the
benchmark is designed to fully load the machine, that means that either
there is not enough total memory for the benchmark, or some other
process(es) are using significant memory. But it can be used if the
benchmark otherwise fails and you just need a result.

### Platform architectures

The underlying `pycbc_inspiral` processes that are started are
designed to be limited by the performance of a complex-to-complex,
single-precision, inverse FFT of length 2^20. Since the goal of
a benchmark is usually to determine the optimal possible
performance of the machine under full load conditions, some
care is taken in testing multiple FFT libraries, and
customizing for the platform on which the benchmark runs.
At present, the two supported platforms are Intel (R) and
AMD (R), and two different FFT libraries will be tried for each.
Each will be run with the [FFTW](https://www.fftw.org) library,
but compiled with more aggressive planning options than the
`conda-forge` compiled distribution (see [these lines](https://git.ligo.org/joshua.willis/fftw-feedstock/-/blob/intel_opt/recipe/build.sh#L37-39) to see what exactly is changed).

Additionally, for AMD (R), we also compile the 
[customized AMD version](https://github.com/amd/amd-fftw/)
of FFTW, and benchmark that as well. For Intel (R) we
also benchmark with the MKL library, as
distributed by `conda-forge`.

The customized FFTW conda packages, as well as `numa_benchmarks`,
are currently distrbuted in a somewhat fragile manner from
a LIGO CIT URL under the developer's personal account. So
also are some auxiliary files needed in the image for the
benchmark to run. Hopefully this will improve in the future.

The nature of FFTW is that it attempts to "plan" a
particular transform, with the goal of finding the optimal
way to perform it for given hardware. It is possible to
save this "wisdom" (that is the library's terminology)
to a file and load it for later reuse, to avoid the
extensive planning time.  This benchmark takes this
into account, and therefore when running an FFTW-based
benchmark, it first creates a plan (under maximal load
conditions) and saves it to a file, and that file is then
loaded by each actual benchmark process. This of course
adds complexity and time to the overall benchmark.

Thus, to summarize, for each of the two supported
platforms, two different benchmarks are performed,
preceeded by wisdom creation if the benchmark uses
FFTW for the FFT library. Those benchmarks are:

1. For AMD (R): `fftw_igwn_opt` and `fftw_amd_fast_plan`
1. For Intel (R): `fftw_igwn_opt` and `fft_mkl`

There are conda environments of each name above that
the benchmark orchestrator enters prior to running
each benchmark, and subdirectories of the platform
directory of the results (as described below) with
the same names.

### Running the benchmark

The benchmark is designed to be run from the CI built
docker containers. First, the system has to be prepared,
and then the correct container run.

#### Prerequisites

Before running the benchmark, you need the following
configured on your benchmark machine:

1. You must have a sufficiently recent `docker`, and
the machine must have internet access to pull the
docker image. This was tested with Docker 20.10.12.
1. You must have CVMFS installed and configured to mount
the GWOSC frames files. You could look at the first
few lines of the Dockerfile in this repository for some
guidance, or talk to your local expert.
1. Alternately, you could ensure that
the needed files will appear correctly inside the
container by copying just those files into a location
so that whatever volume you bind to `/cvmfs` inside the
container, the following files will be found:
    ```
    /cvmfs/gwosc.osgstorage.org/gwdata/O3a/strain.16k/frame.v1/H1/1241513984/H-H1_GWOSC_O3a_16KHZ_R1-1242443776-4096.gwf
    /cvmfs/gwosc.osgstorage.org/gwdata/O3a/strain.16k/frame.v1/H1/1241513984/H-H1_GWOSC_O3a_16KHZ_R1-1242439680-4096.gwf
    ```
    It is probably wisest to `rsync` this, to avoid
    unnoticed file corruption during transfer.

#### Running the containers

The following is a quick guide to what you need to do
once the prerequisites above are met.

1. Change to a working directory where you will run the
benchmark. The container will create files here, so
you need to have write access.
1. Create a script to drive the appropriate container
image, based on whether your machine is Intel (R) or
AMD (R). The benchmark itself will make no attempt to
determine this, so if you use the wrong image you may
at best get meaningless results, or at worst a `SIGILL`.

   For Intel (R), create a script `run_pycbc_bmk_intel.sh`:

    ```
    #!/bin/bash

    BENCHMARK_IMAGE="containers.ligo.org/joshua.willis/pycbc_offline_benchmark/intel:latest"
    docker run --cap-add SYS_NICE -v /cvmfs:/cvmfs -v $PWD:/results -i ${BENCHMARK_IMAGE}
    ```

    whereas for AMD (R) create `run_pycbc_bmk_amd.sh`:
 
    ```
    #!/bin/bash

    BENCHMARK_IMAGE="containers.ligo.org/joshua.willis/pycbc_offline_benchmark/amd:latest"
    docker run --cap-add SYS_NICE -v /cvmfs:/cvmfs -v $PWD:/results -i ${BENCHMARK_IMAGE}
    ```

    The only difference between the scripts is in the container name,
    right before the `:latest` at the end. Note that you must add the
    `--cap-add SYS_NICE` for the benchmark process inside the container
    to set the memory policy and CPU affinity as it expects; if it cannot
    do so, the benchmark will fail.

1. Make the appropriate script executable:

    ```
    chmod +x run_pycbc_bmk_intel.sh

    or

    chmod +x run_pycbc_bmk_amd.sh
    ```

1. Run the benchmark in the background, capturing its
`stdout` and `stderr`.

    ```
    nohup ./run_pycbc_bmk_intel.sh </dev/null 1>out.txt 2>&1 &
    
    or
    
    nohup ./run_pycbc_bmk_amd.sh </dev/null 1>out.txt 2>&1 &
    ```

    If you prefer, you could start docker directly from the
    command line rather than in a script, with `-d` instead
    of `-i`, and follow its progress with `docker logs`. In
    that case you would probably find it convenient to give
    the container a name,  and more work would likely be
    required to capture the container `stdout/stderr` after
    it exits.

### Benchmark results

When the container exits, if it has done so
successfully, you will have in your run directory
a hierarchy of the form:
```
pycbc_offline_benchmark_<date>_<time>/<platform>/<fft_library>/benchmark
```
where `<platform>` is one of `amd` or `intel`, and
there will be two different `<fft_library>` subdirectories:
for AMD (R), there will be `fftw_igwn_opt` and `fftw_amd_fast_plan`;
and for Intel (R), there will be `fftw_igwn_opt` and `fft_mkl`.

Inside each `benchmark` directory will be a file named
`pycbc_offline_benchmark_results.json`. This is the main
result file, and contains the figure of merit from each
benchmark run. For the PyCBC benchmark, that figure of
merit is `tempaltes_per_core`. The `.json` results file
should be formatted to that it can be easily read into
a `pandas` dataframe, and from there exported to your
format of choice. You may also use it to calculate
whatever summary statistics you deem useful, depending
on what application you have for your benchmark.

As an example, if you have a python ecosystem where both
`pandas` and `openpyxl` are installed, then you should be
able to parse Intel (R) results with the following code:

```
import pandas as pd

df1 = pd.read_json("fft_mkl/benchmark/pycbc_offline_benchmark_results.json”)
df2 = pd.read_json("fftw_igwn_opt/benchmark/pycbc_offline_benchmark_results.json”)

with pd.ExcelWriter(‘my_benchmark_results.xlsx') as writer:
    df1.to_excel(writer, sheet_name="MKL results", index=False)
    df2.to_excel(writer, sheet_name="IGWN opt FFTW results", index=False)
```

It is straightforward to instead modify these commands
for benchmarks run on the AMD (R) platform.

Under the `benchmark` directory will also be
subdirectories for each job that ran, where they will
have written their individual outputs (as HDF files)
and their combined `stdout` and `stderr`. The latter
might be useful for debugging, or also to confirm that
the CPU affinity was set as expected, since each job
should be configured to report that.

If the benchmark used FFTW as the FFT library, then in
parallel to the `benchmark` directory will also be
a `wisdom` directory. It will also have job subdirectories,
as well as the wisdom file that was generated in the
inital step of the benchmark and used for the actual
benchmark runs; you may wish to save it for future use.
